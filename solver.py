import obrob_zdj
import szukaj
import re


def rozwiaz(sudoku):
    try:
        x = sudoku.index(0)
    except:
        return sudoku

    pasujace = [sudoku[n] for n in range(81) if not ((x-n)%9 * (x//9^n//9) * (x//27^n//27 | (x%9//3^n%9//3)))]

    for potencjalna in range(1, 10):
        if potencjalna not in pasujace:
            aktualne = rozwiaz(sudoku[:x]+[potencjalna]+sudoku[x+1:])
            if aktualne is not None:
                return aktualne


if __name__ == "__main__":
    obrob_zdj.przetworz()
    sudoku = szukaj.przetlumacz()
    rozwiazanie = rozwiaz(sudoku)
    listToStr = ' '.join([str(elem) for elem in rozwiazanie])
    print("\n\n")
    print(re.sub("(.{18})", "\\1\n", listToStr, 0, re.DOTALL))
