from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import cv2
import math
import os
import re
from matplotlib import pyplot as plt
import matplotlib.image as mpimg


# load an image and predict the class
def siec(img):
    # convert to array
    img = img_to_array(img)
    # reshape into a single sample with 1 channel
    img = img.reshape(1, 1, 28, 28)
    # prepare pixel data
    img = img.astype('float32')
    img = img / 255.0
    # load model
    model = load_model('wagi.h5')
    # prediction array
    digit = model.predict(img)
    #print(digit)
    #print(digit.argmax(axis=-1))
    # predict the class
    digit1 = model.predict_classes(img)
    wykryta = digit1[0]
    print(wykryta)
    return wykryta


def korelacja(pobrany):

    def mean2(x):
        y = np.sum(x) / np.size(x)
        return y

    def corr2(a, b):
        a = a - mean2(a)
        b = b - mean2(b)
        r = (a * b).sum() / math.sqrt((a * a).sum() * (b * b).sum())
        return r


    pobrany = cv2.resize(pobrany, (60,60))
    pobrany = cv2.cvtColor(pobrany, cv2.COLOR_BGR2GRAY)

    korelacja = []
    cyfry = []
    path = "E:/Pythonowo/cyAky/obrazy"

    for n in (os.listdir(path)):
        ima2 = cv2.imread(path + "/" + n)
        ima2 = cv2.cvtColor(ima2, cv2.COLOR_BGR2GRAY)
        #cv2.imshow("11", ima2)
        #cv2.waitKey(0)

        korelacja.append(corr2(pobrany, ima2))
        cyfry.append(n)

    korelacja = np.asarray(korelacja)
    #print(korelacja)

    cyfra = (cyfry[int(np.argmax(korelacja))])
    podobienstwo = max(korelacja)

    cyfra = re.sub('\.png$', '', cyfra)
    print(cyfra)
    #print(podobienstwo)

    return cyfra,podobienstwo


def przetlumacz():
    sudoku = []
    sciezka = "E:/Pythonowo/cyAky/przetworzone"

    for n in range(len(os.listdir(sciezka))):
        nazwa = sciezka + "/wyciete" + str(n) + ".png"
        img = load_img(nazwa, color_mode = "grayscale", target_size=(28, 28))
        pobrany = cv2.imread(nazwa)
        img1 = mpimg.imread(nazwa)

        czy_pusty = np.array(np.where(img1 == 1))
        ile_bialych = len(czy_pusty[0])
        print(ile_bialych)

        rozpoznana = 0

        if(ile_bialych > 10):
            wynik_siec = siec(img)
            wynik_korelacja, _ = korelacja(pobrany)
            wynik_siec = int(wynik_siec)
            wynik_korelacja = int(wynik_korelacja)

            if (wynik_siec == wynik_korelacja):
                rozpoznana = wynik_siec
            elif (wynik_siec != wynik_korelacja):
                print("odjebao troche, czy liczba na pozycji " + str(n) + " liczac wierszami to", wynik_siec, "czy", wynik_korelacja, "?")
                rozpoznana = int(input())

        elif(len(czy_pusty==0)):
            rozpoznana=0

        sudoku.append(rozpoznana)

    print(sudoku)

    return sudoku