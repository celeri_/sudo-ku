import cv2
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
from PIL import Image, ImageDraw

def centroiduj(listaPkt):
    def centroid(vertexes):
        _x_list = [vertex[0] for vertex in vertexes]
        _y_list = [vertex[1] for vertex in vertexes]
        _len = len(vertexes)
        _x = 0
        _y = 0
        if(_len != 0):
            _x = sum(_x_list) / _len
            _y = sum(_y_list) / _len
        else: pass

        return (int(_x), int(_y))

    srodek = (centroid(listaPkt))
    listaPkt = np.asarray(listaPkt)
    extreme_points=[]

    if (len(listaPkt!=0)):
        extLeft = tuple(listaPkt[listaPkt[:, 0].argmin()])
        extRight = tuple(listaPkt[listaPkt[:, 0].argmax()])
        extTop = tuple(listaPkt[listaPkt[:, 1].argmin()])
        extBot = tuple(listaPkt[listaPkt[:, 1].argmax()])
        extreme_points = [extLeft, extRight, extTop, extBot, srodek]
    return extreme_points


def morfologuj(pobrany):
    h, w, _ = pobrany.shape
    obraz = cv2.resize(pobrany,((h-h%9),(w-w%9)))
    obraz2 = obraz.copy()

    obraz = cv2.cvtColor(obraz, cv2.COLOR_BGR2GRAY)
    obraz = cv2.Canny(obraz,100,200)

    wysokosc, szerokosc, = obraz.shape
    bok_wys = wysokosc//9
    bok_szer = szerokosc//9
    i=0

    for m in range(0, bok_szer * 9, bok_szer):
        for n in range(0, bok_wys * 9, bok_wys):
            wytnij = (obraz[m:m+bok_szer, n:n+bok_wys])

            for k in range(0, int(bok_szer)):
                for l in range (0, int(0.15*bok_wys)):
                    wytnij[k,l] = 0

            for k in range(0, int(bok_szer)):
                for l in range ((int(0.85*bok_wys)), bok_wys):
                    wytnij[k,l] = 0

            for k in range(0, int(0.15*bok_szer)):
                for l in range (0,int(bok_wys)):
                    wytnij[k,l] = 0

            for k in range((int(0.85*bok_szer)), bok_szer):
                for l in range (0,int(bok_szer)):
                    wytnij[k,l] = 0

            kernel = np.ones((5,5), np.uint8)
            dilation = cv2.dilate(wytnij, kernel, iterations=1)

            stare_biale = np.array(np.where(dilation == 255))
            X = stare_biale[1]
            Y = stare_biale[0]

            biale = stare_biale.copy()
            biale[0] = X
            biale[1] = Y

            tablica_bialych = []

            for z in range(len(biale[0])):
                tupla = tuple(biale[:, z])
                tablica_bialych.append(tupla)

            print(tablica_bialych)
            ekstremalne = centroiduj(tablica_bialych)
            print(ekstremalne)
            print(len(tablica_bialych), "|         " + str(i))
            if(len(tablica_bialych) != 0):
                lewyGora = tuple(map(min, zip(*tablica_bialych)))
                h = abs(ekstremalne[3][1] - ekstremalne[2][1])
                w = abs(ekstremalne[1][0] - ekstremalne[0][0])
                dilation = dilation[(lewyGora[1] - 5):(lewyGora[1] + h + 5), (lewyGora[0] - 5):(lewyGora[0] + w + 5)]
            else:
                pass

            cv2.imwrite(("tnij" + str(i) + ".png"), dilation)

            dziel_kwadrat = cv2.rectangle(obraz2, (m, n), (m + bok_szer, n + bok_wys), (0, 255, 0), 1)
            i+=1

    return bok_wys, bok_szer


def wycentruj(h, w):
    for n in range(81):
        img = Image.open("tnij" + str(n) + ".png", 'r')
        img_w, img_h = img.size
        background = Image.new('L', (h, w), 0)
        bg_w, bg_h = background.size
        offset = ((bg_w - img_w) // 2, (bg_h - img_h) // 2)
        background.paste(img, offset)
        background.save("E:/Pythonowo/cyAky/przetworzone/wyciete" + str(n) + ".png")


def przetworz():
    pobrany = cv2.imread('sudoku1.jpg')
    h,w = morfologuj(pobrany)
    wycentruj(h, w)


przetworz()